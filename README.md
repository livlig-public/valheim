# Users
[How to connect to the server](https://gitlab.com/livlig-public/valheim/-/wikis/Connecting-to-the-server)

If the server is using Valheim Plus you must [setup Valheim Plus](https://gitlab.com/livlig-public/valheim/-/wikis/Valheim-Plus-Client-Setup) on your PC first.

# Deploy a Server
Deploy using [Docker template](https://gitlab.com/livlig-public/docker) or see 

If using Valheim Plus, place the Valheim Plus Config in `./config/valheimplus/valheim_plus.cfg`

# More Info
[Valheim server Docker image](https://github.com/lloesche/valheim-server-docker)  
[Valheim Plus](https://valheim.plus/documentation)
